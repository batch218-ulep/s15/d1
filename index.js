console.log("Hello World");
// enclose with qoutation mark string data

console. log("Hello World");
// not space and line sensitive

console.
log
(
	"Hello Everyone"
);

// ; delimiter - to end our code

// [COMMENTS]
// - Single line comments
// Shortcut: Ctrl + /

// I am a single line comment

/* Multiline comment */
/*
	I am
	a
	multiline
	comment
*/
/* Shortcut: Ctrl + Shft + / */

// Syntax and Statement

// Statements in programming are instruction that we tell to computer to perform
// Syntax in programming, it is the set of rules that describes how statements must be considered

// Variables
/*
	it is used to contain data

	-Syntax in declaring variables
	- let/constant variable Name
*/

let myVariable = "Hello";
			//assignment operator (=);
console.log(myVariable);
// console.log(Hello); // will result to "not defined" error

/*
	Guide in writing variables:
		1. Use the "Let" keyword followed by the variables name of your choosong and use the assign operator (=) to assign a value.
		2. Variable names should start with a lowercase character, use camelcase for multiple words.
		3. For constant variables, use the "const" keyword
		4. Variables name should be indicative or descriptive of the value being stores
		5. Never name a variable starting with numbers

		sample: let 2temperature = 82;

		6. Refrain from using space in declaring a variable
*/

// String
let productName = 'desktop computer';
console.log(productName);

// double or single quote can be use
let product = "Alvins computer";
console.log(product);

let productPrice = 18999;
console.log(productPrice)

const interest = 3.539;
console.log(interest);

// Reassigning variable value
// Syntax
	// variablename = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend="Jane";
console.log(friend);

// interest=4.89; will get an error

const pi=3.14;
console.log(pi)
// pi=3.16; constant value cannot be change

// Reassigning - a variable already have a value and we are re-assign a new one
// Initialize - it is our first saving of a value

let supplier; //declaration
supplier = "John Smith Trading"; //initialize
console.log(supplier);

supplier="Zuitt Store";
console.log(supplier);

// Multiple variable Declaration

let productCode="DC017";
const productBrand='Dell';
console.log(productCode, productBrand);

// Using a variable with reserved keyword
// const let='hello';
// console.log(let); 

// [Section] data types

// Strings
// Strings are series of character that create a word, phrase, sentence, or anything related to creating text.
// string in javaScript is enclose with single ('') or double quote ("")

let country='Philippines';
let province='Metro Manila';

let fullAddress=province + ', ' +country;
// plus (+) symbol to concatenate data/value

console.log(province+', '+country);
console.log(fullAddress);

console.log("Philippines" + ', ' + "Metro Manila");

// Escape Character (\)
// "\n" referes to creating a new line or set the text to next line

console.log("line1\nline2");

let mailAddress='Metro Manila\nPhilippines';
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);

message = "John\'s employee went home early.";
console.log(message);

// Numbers
// Integers / Whole numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers / Float / Fraction
let grade=74.9;
console.log(grade)

// Exponetial Notation
let planetDistance  = 2e10;
console.log(planetDistance);

console.log("Jhon's grade last quarter is " + (2 + grade));

// Arrays
// it is store multiple values with simillar data type

let grades=[98.7, 95.4, 90.2, 94.6];
console.log(grades);

// different data types
// Storing diiferent data types inside an array is not recommended because it will not make sense in the context of programming.
let details=["John", "Smith", 32, true];
console.log(details);

// Objects
// -are another special kind of data type that is used to mimic real world objects/items

// Syntax:
/*
	let/const objectName={
		propertyA: value,
		propertyB: value
	}
*/

let person= {
	fullName: "Juan Dela Cruz",
	// key/property
	age: 35,
	isMarried:false,
	contact: ["0912 345 6789", "8123 7444"],
	/*address: {
		houseNumber: "345",
		city: "Manila"
	}*/
};

console.log(person);

const myGrades={
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

// Type of Operators
let stringValue ="string";
let numberValue =10;
let booleanValue =true;
let waterBills =[1000, 640, 700];
// myGrades as object


// 'typeof' -
// use type of operator to retrieve / know the data type

console.log(typeof stringValue); //output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); //output: boolean
console.log(typeof waterBills); //output: object
console.log(typeof myGrades); //output: object

// Multiple data is identify as object

// Constant Object and Arrays
// We cannot reassign the value of the variable but we can change the elements of the constant array

// index - is the position of the element starting zero
const anime = ["OnePiece", "Black Clover", "Monster", "Demon Slayer", "Fairy Tale", "Boruto"];
anime[5] = "Naruto";
console.log(anime);

// Null
// It is used to intentionally expenses the absence of a value
let spouse=null;
console.log(spouse);

// Undefined
	// Represents the state of a variable that has been declared but without an assigned value.
	let fullName;
	console.log(fullName);

	// Undefined vs Null
	// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
	// null means that a variable was created and was assigned a value that does not hold any value/amount
	// Certain processes in programming would often return a "null" value when certain tasks results to nothing
















